import java.util.Scanner;

public class AB_Zinseszins {

	public static void main(String[] args) {
		Scanner ms = new Scanner(System.in);

		
		//////////////////////////////////////////
		// abfrage[0] => Laufzeit				//
		// abfrage[1] => Eingezahltes Kapital	//
		// abfrage[2] => Zinssatz				//
		//////////////////////////////////////////
		Double[] abfrage = AskForZinsStuffToCalculate(ms);
		
		Double kZ = processing(abfrage[0], abfrage[1], abfrage[2]);
		
		System.out.println("");
		System.out.println("=============================");
		System.out.printf("Eingezahltes Kapital: %.2f Euro \n", abfrage[1]);
		System.out.printf("Ausgezahltes Kapital: %.2f Euro \n", kZ);
	}
	
	public static Double[] AskForZinsStuffToCalculate(Scanner ms) {
		Double[] aZ = new Double[4];
		// Laufzeit Variable => Füllen
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		aZ[0] = ms.nextDouble();
						
		// Kapital Variable => Füllen
		System.out.print("Wie viel Kapital (in Euro) möchten Sie anlegen: ");
		aZ[1] = ms.nextDouble();
		
		// Zinssatz variable => Füllen
		System.out.print("Zinssatz: ");
		aZ[2] = ms.nextDouble();
		return aZ;
	}
	// lF => Laufzeit
	// eK => eingezahltes Kapital
	// zS => Zinsatz
	public static double processing(Double lF, Double eK, Double zS) {
		int zaehler = 0;
		double kZ = eK;
		
		while (zaehler < lF) {
			double zinsBetrag = kZ / 100 * zS;
			kZ += zinsBetrag;
			zaehler++;
		}
		
		return kZ;
	}

}
