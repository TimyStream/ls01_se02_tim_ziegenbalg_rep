
public class Aufgabe3 {

	public static void main(String[] args) {
		rowHead("Fahrenheit", "Celsius");
		System.out.print("-----------------------\n");
		row("-20", -28.8889);
		row("-10", -23.3333);
		row("0", -17.7778);
		row("+20", -6.6667);
		row("+30", -1.1111);
		
	}

	public static void rowHead(String zeile1, String zeile2) {
		System.out.printf("%-12s", zeile1);
		System.out.printf("%s", "|");
		System.out.printf("%10s", zeile2);
		System.out.print("\n");
	}
	
	public static void row(String zeile1, double zeile2) {
		System.out.printf("%-12s", zeile1);
		System.out.printf("%s", "|");
		System.out.printf("%10.2f", zeile2);
		System.out.print("\n");
	}
}
