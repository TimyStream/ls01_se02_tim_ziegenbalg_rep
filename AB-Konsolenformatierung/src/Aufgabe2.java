
public class Aufgabe2 {

	public static void main(String[] args) {
		row("0!", " ", "1");
		row("1!", "1", "1");
		row("2!", "1 * 2", "2");
		row("3!","1 * 2 * 3", "6");
		row("4!", "1 * 2 * 3 * 4", "24");
		row("5!", "1 * 2 * 3 * 4 * 5", "120");
	}
	
	public static void row(String zeile1, String zeile2, String zeile3) {
		System.out.printf("%-5s", zeile1);
		System.out.printf("%s", "= ");
		System.out.printf("%-19s", zeile2);
		System.out.printf("%s", "= ");
		System.out.printf("%4s", zeile3);
		System.out.println("\n");
	}
}
