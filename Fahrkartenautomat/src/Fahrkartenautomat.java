﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		// Scanner tastatur = new Scanner(System.in);

		String[] tNames = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };

		double[] tPreise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };

		/*
		 * Aufgabe Nr. 1) Durch das zusammen fassen in Array haben wir den Vorteil, wir
		 * können Menüs Automatisch erstellen auf basis des Array durch die Index
		 * Nummer.
		 * 
		 * Aufgabe Nr. 2) Durch die Implementierung der Arrays gibt es die möglichkeit,
		 * dass wir eifach neue Fahrkarten Hinzufügen können bzw. welche Löschen können
		 * ohne dass wir neue einträge erstellen müssen.
		 */

		while (true) {
			double zuZahlenderBetrag = 0;
			double eingezahlterGesamtbetrag = 0;

			boolean firstTicket = true;
			// -----------------
			// Bestellerfassung
			// -----------------
			zuZahlenderBetrag = fahrkartenBestellungMenu(firstTicket, tNames, tPreise) * 100;
			// ------------
			// Geldeinwurf
			// ------------
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			// ------------------
			// Fahrscheinausgabe
			// ------------------
			fahrkartenAusgabe();
			// --------------------------------
			// Rückgeldberechnung und -Ausgabe
			// --------------------------------
			rueckgeldAusgabe((eingezahlterGesamtbetrag - zuZahlenderBetrag));
		}
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		eingezahlterGesamtbetrag = 000;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("%s %.2f %s%n", "Noch zu zahlen:", (zuZahlenderBetrag - eingezahlterGesamtbetrag) / 100,
					"Euro");
			// System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag -
			// eingezahlterGesamtbetrag));
			sop("Eingabe (mind. 5Ct, höchstens 10 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble() * 100;
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 15; i++) {
			sop("=");
			warte(250);
		}
		sopl("\n\n");
	}

	public static void rueckgeldAusgabe(double rückgabebetrag) {
		if (rückgabebetrag > 005) {
			System.out.printf("%s %.2f %s%n", "Der Rückgabebetrag in Höhe von", rückgabebetrag / 100, "Euro");
			// System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + "
			// EURO");
			sopl("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 200) // 2 EURO-Münzen
			{
				rückgabebetrag = muenzeAusgeben(rückgabebetrag, 200, "Euro");
			}
			while (rückgabebetrag >= 100) // 1 EURO-Münzen
			{
				rückgabebetrag = muenzeAusgeben(rückgabebetrag, 100, "Euro");
			}
			while (rückgabebetrag >= 50) // 50 CENT-Münzen
			{
				rückgabebetrag = muenzeAusgeben(rückgabebetrag, 50, "Cent");
			}
			while (rückgabebetrag >= 20) // 20 CENT-Münzen
			{
				rückgabebetrag = muenzeAusgeben(rückgabebetrag, 20, "Cent");
			}
			while (rückgabebetrag >= 10) // 10 CENT-Münzen
			{
				rückgabebetrag = muenzeAusgeben(rückgabebetrag, 10, "Cent");
			}
			while (rückgabebetrag >= 5)// 5 CENT-Münzen
			{
				rückgabebetrag = muenzeAusgeben(rückgabebetrag, 5, "Cent");
			}
		}

		sopl("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static double muenzeAusgeben(double rückgabebetrag, int betrag, String einheit) {
		if (einheit == "Euro") {
			sopl((betrag / 100) + " " + einheit);
			return rückgabebetrag -= betrag;
		} else {
			sopl(betrag + " " + einheit);
			return rückgabebetrag -= betrag;
		}
	}

	public static double fahrkartenBestellungMenu(boolean firstTicket, String[] tNames, double[] tPreise) {
		Scanner tastatur = new Scanner(System.in);

		int auswahl;
		int anzahlTickets;

		double kartenPreis = 0.00;
		double zuZahlenderBetrag = 0.00;

		boolean bezahlen = false;

		while (bezahlen == false) {

			kartenPreis = 0.00;

			sopl("Fahrkartenbestellvorgang:");
			sopl("=========================");
			eLine();
			sopl("Wählen sie Ihre wunsch Fahrkarte aus: ");
			for (int i = 0; i < tNames.length; i++) {
				sopl("   " + tNames[i] + " (" + i + ")");
			}
			if (firstTicket == false)
				sopl("   Bezahlen (" + tNames.length + ")");
			eLine();
			sop("Ihre Wahl: ");
			auswahl = tastatur.nextInt();
			if (auswahl == tNames.length) {
				bezahlen = true;
				continue;
			}
			eLine();
			sop("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();

			if (!(anzahlTickets <= 10)) {
				while (!(anzahlTickets <= 10)) {
					sopl("ERROR! Ungültige eingabe!");
					sop("Anzahl der Tickets: ");
					anzahlTickets = tastatur.nextInt();
				}
			}
			kartenPreis = tPreise[auswahl];

			firstTicket = false;

			zuZahlenderBetrag += kartenPreis * anzahlTickets;
		}
		return zuZahlenderBetrag;
	}

	// #########################
	// Extra Stuff
	// #########################
	public static void sop(String print) {
		System.out.print(print);
	}

	public static void sopl(String print) {
		System.out.println(print);
	}

	public static void eLine() {
		sopl("");
	}
}