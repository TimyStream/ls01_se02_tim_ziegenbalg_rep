import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
	   Scanner tastatur = new Scanner(System.in);
      // (E) "Eingabe"
      // Werte fuer x und y festlegen:
      // ===========================
      double x;
      double y;
      
      
      ///////////////////////////////////
      sopl("#########################");
      sopl("#      Made with <3     #");
      sopl("#         Made by       #");
      sopl("# Tim Sidney Ziegenbalg #");
      sopl("#########################");
      ///////////////////////////////////
      
      
      while (true) {
    	  sop("Geben sie bitte eine Zahl ein: ");
    	  x = tastatur.nextDouble();
    	  sop("Geben sie bitte eine Zweite Zahl ein: ");
    	  y = tastatur.nextDouble();
    	  System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f %n", x, y, mw(x, y));
    	  sopl("#########################################################");
    	  
    	  x = 0;
    	  y = 0;
      }
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, mw(x, y));
   }
   public static void sopl(String print) {
	   System.out.println(print);
   }
   public static void sop(String print) {
	   System.out.print(print);
   }
   public static double mw(double x, double y) { // Berechne den Mittelwert Funtion
	   double m = (x + y) / 2.0;
	   return m;
   }
}
