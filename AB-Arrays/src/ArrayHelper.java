
public class ArrayHelper {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		
		System.out.println(convertArrayToString(OneParameterMethod(array)));
		System.out.println("===============================================");
		System.out.println(convertArrayToString(OPMnF(array)));
		System.out.println();
		
		
		float test = 5;
		System.out.println(5/9*test-32);
		
		System.out.println();
		
		double[][] temp = TemperaturUmwandlung(5);
		System.out.printf("%-12s%s%10s\n", "Fahrenheit", "|", "Celsius");
		System.out.println("-----------------------");
		for(int i = 0; i < temp[1].length; i++) {
			//System.out.println("Fahrenheit: " + temp[0][i] + " | Celsius: " + temp[1][i]);
			System.out.printf("%-12.2f", temp[0][i]);
			System.out.printf("%s", "|");
			System.out.printf("%10.2f", temp[1][i]);
			System.out.print("\n");
		}
		
	}
	
	public static String convertArrayToString(int[] array) {
		String arrayString = "Array: ";
		
		for (int i = 0; i < array.length; i++) {
			arrayString += array[i] + ", ";
		}
		
		return arrayString;
	}
	
	public static int[] OneParameterMethod(int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = array.length - i;
		}
		
		return array;
	}
	
	public static int[] OPMnF(int[] array) {
		 int[] newArray = new int [array.length];
		 
		 for (int i = 0; i < array.length; i++) {
			 newArray[i] = array[array.length - (i+1)];
		 }
		
		return newArray;
	}
	
	public static double[][] TemperaturUmwandlung(int anzahlWertePaare) {
		
		// double[0][XX] => Fahrenheit
		// double[1][XX] => Celsius
		
		double[][] array = new double[2][anzahlWertePaare];
		
		array[0][0] = 0.0;
		//array[1][1] = 10.00;
		
		for (int i = 0; i < array[0].length; i++) {
			array[1][i] = (5.0 / 9.0)* (array[0][i] -32);
			if (!(i == anzahlWertePaare-1)) {
				array[0][i+1] = array[0][i] + 10; 
			}
		}
		
		
		return array;
	}

}