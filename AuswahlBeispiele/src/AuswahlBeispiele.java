
public class AuswahlBeispiele {

	public static void main(String[] args) {
			
	}
	
	public static void drawMuenze(int betrag, String einheit, byte counter) {
    	while(counter < 3) {
    		System.out.printf("   * * *  ");
    	}
    	
    	sopl("   * * *  ");
		sopl(" *       * ");
		if(einheit == "Euro") {
			System.out.printf("%-4s %d %4s \n", "*", betrag, "*");	
		} else {
			if(betrag < 10) {
				System.out.printf("%-4s %d %4s \n", "*", betrag, "*");
			} else {
				System.out.printf("%-3s %d %4s \n", "*", betrag, "*");
			}
		}
		sopl("*  "+ einheit +"   *");
		sopl(" *       * ");
		sopl("   * * *  ");
    }
	
	
	
	
    // #########################
    // Extra Stuff
    // #########################
    public static void sop(String print) {
    	System.out.print(print);
    }
    public static void sopl(String print) {
    	System.out.println(print);
    }
    public static void eLine() {
    	sopl("");
    }
}
