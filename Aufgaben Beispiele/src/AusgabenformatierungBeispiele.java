
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		final String zahlenketteString = "123456789";
		final int zahlenketteINT = 1234556789;
		final double zahlenketteFloat = 12345.6789;
		
		/*
		System.out.println(zahlenketteString);
		System.out.print(zahlenketteINT + "\n");
		System.out.print(zahlenketteFloat);
		*/
		/*
		System.out.printf("|%-20.3s|%n", "123456789");
		System.out.printf("|%-20.3s|%n", "1465748");
		*/
		
		System.out.printf("|%d|%n", zahlenketteINT);
		System.out.printf("|%-20d|%n", zahlenketteINT);
		System.out.printf("|%-20d|%n", zahlenketteINT);
		
		/*
		System.out.printf("|%f|%n", zahlenketteFloat);
		System.out.printf("|%.2f|%n", zahlenketteFloat);
		System.out.printf("|+%-20.2f|%n", zahlenketteFloat);
		System.out.printf("|-%-20.2f|%n", zahlenketteFloat);
		*/
		
		final String mon = "Monitor";
		final double betragMonitor = 109.50;
		
		System.out.printf("Der Preis von %s betr�gt %.2f Euro %n", mon, betragMonitor);
	}

}
